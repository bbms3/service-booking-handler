package com.bbms.bookinghandler.bookinghandler.builder;

import com.bbms.bookinghandler.bookinghandler.config.ObjectStoreConfig;
import io.minio.MinioClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class ObjectStoreBeanBuilder {

    @Autowired
    ObjectStoreConfig objectStoreConfig;

    @Bean
    public MinioClient getMinioClient(){
        MinioClient minioClient =
                MinioClient.builder()
                        .endpoint("http://"+objectStoreConfig.getHost()+":"+objectStoreConfig.getPort())
                        .credentials(objectStoreConfig.getAccessKey(), objectStoreConfig.getSecretKey())
                        .build();
        return minioClient;
    }
}
